# qacube-formula1-statistics-​NikolaPavlovic


**Overview:**

Koriscenjem Erdgast API-ja za Formulu 1 (http://ergast.com/mrd/) uraditi sledece:

1. napraviti model baze koji podrzava podatke za sezonu, trku, vozaca, kvalifikacije i rezultate trke
2. importovati podatke za sezonu 2015, 2016 i 2017 iz api-ja u model
3. napraviti API koriscenjem RESTful web servisa koji za izabranu sezonu daje podatke za maksimalno, minimalno i prosecno vreme kvalifikacionih trka


Preporuceni tech stack:

- Java 8, Spring (Spring Boot, Spring Data ...) OR Node.js
- MySQL for storage,
- JavaScript ES6, Typescript
- Angular 2+,
- SASS



**Bonus ​ ​zadatak​:**


- izracunati maksimalno, minimalno i prosecno vreme kvalifikacionih trka bez koriscenja edb query-ja
